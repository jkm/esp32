#!/bin/sh


BUILD_DIR="tmp"

ENV_BIN="$PWD/env/bin"
ESP_BIN="$HOME/.espressif/tools/xtensa-esp32-elf/esp-2021r2-8.4.0/xtensa-esp32-elf/bin"
IDF_PATH="$PWD/idf"
CMAKE_FLAGS="-DPYTHON_DEPS_CHECKED=1 -DESP_PLATFORM=1 -DIDF_TARGET=esp32 -DCCACHE_ENABLE=0"
ENV="IDF_PATH=$IDF_PATH PATH=$PATH:$ENV_BIN:$ESP_BIN"


# Validate source path
SRC_DIR=$1
if [ -z $SRC_DIR ]; then
  echo "Source path not provided"
  exit 1
fi

if [ ! -d $SRC_DIR ]; then
  echo "Source path does not exist"
  exit 1
fi

# Remove old build files
rm -rf $BUILD_DIR
mkdir $BUILD_DIR

# Generate build files
env $ENV cmake -G 'Unix Makefiles' $CMAKE_FLAGS $SRC_DIR -B $BUILD_DIR

# Build project
env $ENV make -C $BUILD_DIR
