#!/bin/sh


ENV_DIR="env"
IDF_DIR="idf"

IDF_VERSION="v4.4"
IDF_FILE="esp-idf-${IDF_VERSION}.zip"
IDF_URL="https://dl.espressif.com/github_assets/espressif/esp-idf/releases/download/${IDF_VERSION}/${IDF_FILE}"


# Install packages
apt update
apt install build-essential python3 python3-dev python3-pip python3-virtualenv wget unzip -y

# Create virtual environment
virtualenv -q $ENV_DIR

# Upgrade pip
./$ENV_DIR/bin/pip3 -q install -U pip

# Get ESP-IDF archive
if [ ! -f $IDF_FILE ]; then
  wget -q $IDF_URL
fi

# Unpack ESP-IDF archive
unzip -q $IDF_FILE

# Rename ESP-IDF directory
mv esp-idf-$IDF_VERSION $IDF_DIR

# Remove .git directory
rm -rf $IDF_DIR/.git

# Install Python requirements
sed -i '40,45d' $IDF_DIR/requirements.txt
./$ENV_DIR/bin/pip3 -q install -r $IDF_DIR/requirements.txt

# Install esptool
./$ENV_DIR/bin/pip3 -q install esptool

# Install xtensa toolchain
./$ENV_DIR/bin/python3 $IDF_DIR/tools/idf_tools.py install --targets=esp32

# Remove Python bytecode
find /usr/lib/python3 -name *.pyc -delete
find /usr/lib/python3 -name __pycache__ -delete
find $ENV_DIR -name *.pyc -delete
find $ENV_DIR -name __pycache__ -delete

# Remove pip cache
rm -rf $HOME/.cache/pip
