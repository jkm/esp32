#!/bin/sh


ENV_DIR="env"
BUILD_DIR="tmp"

CHIP="esp32"
BAUD=460800
FLAGS="--before=default_reset --after=hard_reset"


# Verify port path
PORT=$1
if [ -z $PORT ]; then
  echo "Port path not provided"
  exit 1
fi

if [ ! -c $PORT ]; then
  echo "$PORT is not a character device"
  exit 1
fi

# Flash the binary
cd $BUILD_DIR
../$ENV_DIR/bin/esptool.py -c $CHIP -p $PORT -b $BAUD $FLAGS write_flash $(cat flash_args)
